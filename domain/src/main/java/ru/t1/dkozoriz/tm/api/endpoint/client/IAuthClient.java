package ru.t1.dkozoriz.tm.api.endpoint.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.user.UserLoginRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dkozoriz.tm.dto.response.user.LoginUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.LogoutUserResponse;
import ru.t1.dkozoriz.tm.dto.response.user.ViewProfileUserResponse;

public interface IAuthClient extends IEndpointClient {
    @NotNull
    LoginUserResponse login(@NotNull UserLoginRequest request);

    @NotNull
    LogoutUserResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    ViewProfileUserResponse getProfile(@NotNull UserViewProfileRequest request);

}