package ru.t1.dkozoriz.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.dto.IAbstractDtoService;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.enumerated.Role;

public interface IUserDtoService extends IAbstractDtoService<UserDto> {

    @NotNull UserDto create(@Nullable String login, @Nullable String password);

    @NotNull UserDto create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull UserDto create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable UserDto findByLogin(@Nullable String login);

    @Nullable UserDto findByEmail(@Nullable String email);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    @NotNull UserDto setPassword(@Nullable String id, @Nullable String password);

    @NotNull UserDto updateUser(@Nullable String id,
                                @Nullable String firstName,
                                @Nullable String lastName,
                                @Nullable String middleName
    );

    @NotNull Boolean isLoginExist(@Nullable String login);

    @NotNull Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unLockUserByLogin(@Nullable String login);

}