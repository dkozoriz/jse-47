package ru.t1.dkozoriz.tm.api.repository.dto;

import ru.t1.dkozoriz.tm.dto.model.SessionDto;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDto> {

}