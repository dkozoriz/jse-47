package ru.t1.dkozoriz.tm.repository.model.business;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.model.IProjectRepository;
import ru.t1.dkozoriz.tm.model.business.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends BusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<Project> getClazz() {
        return Project.class;
    }

}