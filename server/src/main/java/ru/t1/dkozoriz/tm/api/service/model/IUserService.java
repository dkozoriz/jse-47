package ru.t1.dkozoriz.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.model.User;

public interface IUserService extends IAbstractService<User> {

    @NotNull User create(@Nullable String login, @Nullable String password);

    @NotNull User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable User findByLogin(@Nullable String login);

    @Nullable User findByEmail(@Nullable String email);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    @NotNull User setPassword(@Nullable String id, @Nullable String password);

    @NotNull User updateUser(@Nullable String id,
                             @Nullable String firstName,
                             @Nullable String lastName,
                             @Nullable String middleName
    );

    @NotNull Boolean isLoginExist(@Nullable String login);

    @NotNull Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unLockUserByLogin(@Nullable String login);
}
