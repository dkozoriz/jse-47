package ru.t1.dkozoriz.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IAbstractDtoRepository<UserDto> {

    @Nullable UserDto findByLogin(@Nullable String login);

    @Nullable UserDto findByEmail(@Nullable String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}